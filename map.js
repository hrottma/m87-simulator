var telPos = [];
var telPath = [];
var arcs = [];
var satData = [];
var scale = 1;
var globe;
const POV = {lat: 0, lng: -75, altitude:2};     // point of view

function drawGlobe2(container) {
    
    telPos.push({lat:  -90.0, lng: -139.2666656, size:0.1, color: "red"}); // SPT
    telPos.push({lat: 37.0596964279, lng:-3.38896344414, size:0.1, color: "red"}); // PV 
    telPos.push({lat: 31.9533, lng: -111.615, size:0.1, color: "red"}); // SMTO
    telPos.push({lat:  19.8243, lng: -155.478, size:0.1, color: "red"}); //SMA
    telPos.push({lat:  19.8228, lng: -155.477, size:0.1, color: "red"}); //JCMT
    telPos.push({lat: -23.0058, lng: -67.7592, size:0.1, color: "red"}); // APEX
    telPos.push({lat: -23.0234, lng: -67.7538, size:0.1, color: "red"}); // ALMA
    telPos.push({lat: 18.985833, lng: -97.314722, size:0.1, color: "red"}); // LMT

    globe = Globe(animateIn=false)
    .globeImageUrl('images//earth-blue-marble.jpg')
    //.globeImageUrl('images//earth-day.jpg')
    .bumpImageUrl('images/earth-topology.png')
    .backgroundImageUrl('images/night-sky.png')
    .pointsData(telPos)
    .pointAltitude(0.15)
    .pointRadius(0.75)
    .pointsTransitionDuration(0)
    .pointColor('color')
    .pathsData(telPath)
    .pathPoints("pos")
    .pathPointLat((p) => p[0])
    .pathPointLng((p) => p[1])
    .pathPointAlt((p) => p[2])
    .pathTransitionDuration (0)	
    .pathStroke(3)
    .pathColor(() => '#fcae1e')
    .showAtmosphere(true)
    .atmosphereAltitude(0.25)
    .showGraticules(true)
    .objectLat('lat')
    .objectLng('lng')
    .objectAltitude('alt')
    .objectLabel('name')
    .arcsData(arcs)
    .arcAltitude(0)
    .arcStroke(1)
    .arcColor(() => '#fcae1e')
    .arcsTransitionDuration(0)
    (container)

    globe.width(800);
    globe.height(350);

    var control = globe.controls();
    control.enabled = false;

    setTimeout(() => globe.pointOfView(POV, 0));

    const EARTH_RADIUS_KM = 6371; // km
    const SAT_SIZE = 1000; // km

    const satGeometry = new THREE.OctahedronGeometry(SAT_SIZE * globe.getGlobeRadius() / EARTH_RADIUS_KM / 2, 0);
    const satMaterial = new THREE.MeshLambertMaterial({ color: 'red', transparent: true, opacity: 0.7 });
    globe.objectThreeObject(() => new THREE.Mesh(satGeometry, satMaterial));

    var  satData = [{name: "sat1", lat:0, lng:0, alt:1.5}];
    globe.objectsData(satData);

    const globeMaterial = globe.globeMaterial();
    globeMaterial.bumpScale = 10;
    new THREE.TextureLoader().load('images/earth-water.png', texture => {
    
      globeMaterial.specularMap = texture;
      globeMaterial.specular = new THREE.Color('grey');
      globeMaterial.shininess = 15;
    });

    setTimeout(() => { // wait for scene to be populated (asynchronously)
        const directionalLight = globe.scene().children.find(obj3d => obj3d.type === 'DirectionalLight');
        directionalLight && directionalLight.position.set(-400, 150, 60); // change light position to see the specularMap's effect
      });
    
}

function addBaseArcs(pos) {

   // console.log(globe);
    arcs = [];
    for (var i = 0; i < pos.length-1; i++) { 
        for (var j = i+1; j < pos.length; j++) {
            arcs.push( { startLat: pos[i].lat , startLng: pos[i].lng, endLat: pos[j].lat, endLng: pos[j].lng, color: "red"});       
            }
    }
    globe.arcsData(arcs);
}


// scale telescope positions (long/lat) relative to the point-of-view
function scalePositions(scale) {
    
    if (!globe)
        return;
    //console.log("scale: ", scale);

    pos = [];

    // telescope scale should not exceed 1
    if (scale > 1) {
        satScale = scale-1;
        scale = 1;
        pov = POV;
        pov['altitude'] = distValue/12600 +1;
        globe.pointOfView(pov, 0);
    } else
    {
        satScale = -1;
    }

    // scale telescope positions relative to POV
    for (var i = 0; i < telPos.length; i++) {
        tmp = {};
        tmp["lat"] = (telPos[i]["lat"] - POV["lat"])  * scale + POV["lat"];
        tmp["lng"] = (telPos[i]["lng"] - POV["lng"])  * scale + POV["lng"];
        tmp["color"] =  telPos[i]["color"];
        pos.push(tmp);
    }

    addBaseArcs(pos);
    globe.pointsData(pos);

    satData = [
        {name: "space antenna #1", lat: POV['lat'], lng: POV['lng']-90, alt: satScale},
        {name: "space antenna #2", lat: POV['lat'], lng: POV['lng']+90, alt: satScale}
    ];
    globe.objectsData(satData);

    telPath = [
        { pos: [[POV['lat'], POV['lng']-90, satScale], [POV['lat']+90, POV['lng']-90, satScale]]},
        { pos: [[POV['lat'], POV['lng']+90, satScale], [POV['lat']+90, POV['lng']+90, satScale]]},
        { pos: [[POV['lat'], POV['lng']-90, satScale], [POV['lat']-90, POV['lng']-90, satScale]]},
        { pos: [[POV['lat'], POV['lng']+90, satScale], [POV['lat']-90, POV['lng']+90, satScale]]}
        ];


    globe.pathsData(telPath);
}

