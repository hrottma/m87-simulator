function drawSphere(container, tiltAngle, spinVel) {

  tiltAngle = (tiltAngle)/ 180.0 * Math.PI + Math.PI/2;
  spinVel = spinVel / 2000.0;
  var axisColor = "#ece08e";

  //set up the ratio
  var axisLen = 550;
  var sphereSize = 120;

  var rect = container.getBoundingClientRect();
  container.width = rect.width ;
  container.height = rect.height;
  
  //set the scene
  var scene = new THREE.Scene();

  //set the camera
  var camera = new THREE.PerspectiveCamera(80, container.clientHeight   / container.clientWidth );
  camera.position.z = 550;
  camera.position.x = 0;
  camera.position.y = 0;
   camera.rotation.y = 0;

  //set the light
  const dirLlight = new THREE.DirectionalLight(0xffffff,  1);
  dirLlight.position.set(300, 300, 300);
  scene.add(dirLlight);

  const light = new THREE.HemisphereLight(0x111111, 0x000000, 1);
  scene.add(light);

  //  set the renderer 
  var renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true , canvas: container});
  renderer.setViewport(0,0, rect.width, rect.height);

    const axisGroup = new THREE.Group();

    // axis cylinder
    const axis = new THREE.Mesh(new THREE.CylinderGeometry( 10, 10, axisLen, 32 ), new THREE.MeshPhongMaterial( {color: axisColor, shininess: 1} ));
    axisGroup.add(axis);

    // axis pointer
    const cone = new THREE.Mesh( new THREE.ConeGeometry( 25, 55, 32 ),  new THREE.MeshPhongMaterial( {color: axisColor, shininess: 0} ));
    cone.position.y = axisLen / 2;
    cone.rotation.y = Math.PI;
    axisGroup.add( cone );

    // axis torus 
    const torus = new THREE.Mesh( new THREE.TorusGeometry( 30, 5, 16, 100, 4.5 ), new THREE.MeshPhongMaterial( {color: axisColor} ));
    //torus.rotation.x = Math.PI/2;
    torus.position.y = axisLen / 2 - 50;
    torus.lookAt(0,0,0);

    axisGroup.add( torus );

    // torus pointer
    const torusCone = new THREE.Mesh( new THREE.ConeGeometry( 15, 40, 32 ),  new THREE.MeshPhongMaterial( {color: axisColor} ));
    torusCone.position.y = axisLen / 2 - 50 ;
    torusCone.position.x = 30 ;
    torusCone.rotation.x = 1.57 ;
    
    scene.add (axisGroup);

    // sphere
    var geometry = new THREE.SphereGeometry(sphereSize, 30, 30, 0, Math.PI * 2, 0, Math.PI * 2);
    var material = new THREE.MeshPhongMaterial({wireframe: false, color: 0x333333 , flatShading: false,  opacity:1, transparent:false});
    var bh = new THREE.Mesh(geometry, material);
    var grid = new THREE.Mesh(new THREE.SphereGeometry(sphereSize+2, 25, 25, 0, Math.PI * 2, 0, Math.PI * 2), new THREE.MeshPhongMaterial({wireframe: true, color: 0x999999}));

    scene.add(bh);
    scene.add(grid);

   var animate = function () {
    requestAnimationFrame(animate);
    axisGroup.rotation.y += spinVel;
    axisGroup.rotation.x = tiltAngle;
    bh.scale.setScalar(1.7);
    grid.scale.setScalar(1.7);

    grid.rotation.x = 1*tiltAngle;
    grid.rotation.y += spinVel;

    renderer.render(scene, camera);

};

animate();
}
