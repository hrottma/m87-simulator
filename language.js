var en = {
    mass:"Mass",
    distance: "Telescope size",
    spin:"Spin",
    tilt:"Inclination",
    welcome:"Build your black hole",
    wait:"Please wait",
    infomodelimage: `<p style="font-size:20px ;">Black holes are among the most mysterious objects in the Universe</p>
        <p> Around black holes, gravity is extreme and very hot gas orbits them at almost the speed of light. Scientists have developed theories about both the "space-time", or "nature" of the black hole and its surroundings, and the radiation coming from the gas around it. In this way, a black hole and its appearance in the sky can be simulated on a computer. The result is what you see in this panel! </p>
        <p> The sphere in the bottom right-hand corner of this panel shows the orientation of the black hole\'s spin axis relative to our line of sight. You can change the orientation of this spin axis (along with other physical parameters of the black hole model) in the lower left panel to see how these parameters affect the light we see with radio telescopes.</p>`,
    infom87image: `<p style="font-size:20px ;">The famous image of the black hole at the heart of the galaxy M87</p>
        <p> The black hole in M87 has a mass of about 6.5 billion times that of our Sun. Observed by the Event Horizon Telescope in 2017, it took over 300 scientists more than two years to analyze the data and produce this image, which was unveiled to the world in April 2019.<p>`,
    infotelescope: `<p style="font-size:20px ;"> Virtual super-size telescopes</p>
        <p> Radio interferometry links several radio telescopes together to create a "virtual" telescope of much larger size. In the case of the Event Horizon Telescope, this is the size of the entire Earth! </p>
        <p> Each telescope is marked with a red pointer on the globe (their positions are accurate when the slider is set to "1"). The telescopes are connected by yellow lines called "baselines". The longer the baselines, the higher the resolution and the sharper the image!</p>
        <p> By using satelite radio telescopes that orbit Earth we can produce much sharper images than what we can to today.`,
    infocontrols: `<p style="font-size:20px ;">Build your own black hole!</p>
        <p> These sliders allow you to transform the image in the panel above. <br/>
        The <b>inclination</b> is simply the angle from which we view the black hole, in degrees.</p>
        <p> The <b>spin</b> tells us how fast the black hole is rotating. A value of zero means no rotation, and a value of one means maximum rotation.</p>
        <p> The <b>mass</b> slider increases the mass of the black hole - in units of one billion solar masses. A "6.5" on the slider then corresponds to the actual mass of the M87 black hole.</p> 
        <p> The <b>telescope size</b> slider is linked to both the top left and bottom right panels: by increasing the size of the telescope array, we can obtain a much "sharper" image of the black hole. Conversely, with a small array, the image will appear very blurred. For more information, see the info box for the bottom right panel!</p>`
};

var de = {
    mass:"Masse",
    distance: "Teleskopgröße",
    spin:"Rotation",
    tilt:"Sichtwinkel",
    welcome:"Baue ein Schwarzes Loch",
    wait:"Bitte warten",
    infomodelimage: `<p style="font-size:20px ;">Schwarze Löcher gehören zu den seltsamsten Objekten im Universum</p> 
        <p> In der Nähe von Schwarzen Löchern herrscht eine extreme Schwerkraft, und sehr heißes Gas umkreist sie mit nahezu Lichtgeschwindigkeit. Wissenschaftler haben Theorien über die “Raum-Zeit”, bzw. die “Natur” der schwarzen Löcher und ihrer Umgebung, sowie der Strahlung des Gases um sie herum entwickelt. Damit kann das Erscheinungsbild eines schwarzen Lochs am Himmel am Computer simuliert werden. Das Ergebnis sieht man in diesem Panel! </p> 
        <p> Die Kugel in der rechten unteren Ecke dieses Panels zeigt die Orientierung der “Spin-Achse” des schwarzen Lochs relativ zu unserer Sichtlinie. Diese Orientierung kann zusammen mit anderen Parametern im Panel unten verändert werden, um zu sehen, wie sich das Licht verändert, das wir mit Radioteleskopen beobachten würden.</p>`,
    infom87image: `<p style="font-size:20px ;"> Das berühmte Bild des Schwarzen Lochs im Herzen der Galaxie M87</p>
        <p> Dieses schwarze Loch hat eine Masse, die 6.5 Milliarden mal größer ist als die unserer Sonne. Nach den Beobachtungen durch das Event Horizon Telescope im Jahr 2017, arbeiteten über 300 Wissenschaftler mehr als zwei Jahre, um dieses Bild zu erzeugen, das im April 2019 veröffentlicht wurde.</p>`,
    infotelescope: `<p style="font-size:20px ;"> Virtuelle Riesen-Teleskope </p> 
        <p> In der Radiointerferometrie werden mehrere Radioteleskope zu einem “virtuellen”, viel größeren Teleskop zusammengeschaltet. Im Falle des Event Horizon Telescope, entspricht das der Größe unserer gesamten Erde! <p/>
        <p> Jedes Teleskop ist mit einem roten Marker auf dem Globus dargestellt (ihre Positionen entsprechen der Wirklichkeit bei einer Einstellung von “1”). Die Teleskope sind durch gelbe Linien verbunden, den “Basislinien”. Je länger die Basislinien, desto höher die Auflösung und desto schärfer das Bild!<p>
        <p> Benutzt man Weltraum-Teleskope, die die Erde umkreisen, so wird man Bilder mit sehr viel größerer Schärfe erzeugen können, als dies derzeit möglich ist.`,
   
        infocontrols: `<p style="font-size:20px ;">Baue ein schwarzes Loch!</p>
        <p>  Diese Slider transformieren das Bild im oberen Panel. <br/>
             Der <b>Sichtwinkel</b> ist der Winkel in Grad, unter dem man das schwarze Loch sieht.</p> 
        <p> Die <b>Rotation</b> verrät, wie schnell sich das schwarze Loch dreht. Ein Wert von “Null” entspricht einem nicht rotierenden schwarzen Loch, und ein Wert von “Eins” entspricht maximaler Rotation.</p>
        <P> Der <b>Masse</b> Slider verändert die Masse des Schwarzen Lochs - in Einheiten von einer Milliarde Sonnenmassen. Eine “6.5” auf dem Slider entspricht also der tatsächlichen Masse des schwarzen Lochs in M87 (Panel oben rechts).</p>
        <P> Mit dem <b>Teleskopgröße</b>-Slider lässt sich die Größe des Netzwerks aus Teleskopen einstellen. Durch ein größeres Netzwerk lässt sich ein “schärferes” Bild erzeugen. Gleichzeitig erscheint das Bild sehr verschwommen, wenn ein kleineres Netzwerk gewählt wird. Mehr dazu im Panel unten rechts!</p>`
};


function Language(lang)
{
    var __construct = function() {
        if (eval('typeof ' + lang) == 'undefined')
        {
            lang = "de";
        }
        return;
    }()

    this.getStr = function(str, defaultStr) {
        var retStr = eval('eval(lang).' + str);
        if (typeof retStr != 'nicht definiert')
        {
            return retStr;
        } else {
            if (typeof defaultStr != 'nicht definiert')
            {
                return defaultStr;
            } else {
                return eval('de.' + str);
            }
        }
    }
}