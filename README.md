# M87 simulator 


The M87 simulator is a javascript based application that lets users  try to reproduce the EHT image of M87 by interactively
changing Black Hole parameters as well as the size of the telescope network.
Please note that this application does not present scientifically accurate simulations but
is based on simple image manipulations. It is intended for public outreach purposes only and not
for scientific evaluation.


![IMAGE_DESCRIPTION](images/screenshot.png)


## Getting started

In order to execute the code a computer running a web server is required.

Due to the large number of images that will be preloaded on startup, the initial starting
time of the application can be on the order of minutes depending on the computing hardware
it is executed on.


